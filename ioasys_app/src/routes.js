import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import SignIn from '~/pages/SignIn';
import Home from '~/pages/Home';

export default createAppContainer(
  createStackNavigator(
    {
      SignIn: {
        screen: SignIn,
        navigationOptions: () => ({
          header: null,
        }),
      },
      Home: {
        screen: Home,
        navigationOptions: () => ({
          title: 'Enterprises',
        }),
      },
    },
    {
      defaultNavigationOptions: {
        gesturesEnabled: false,
        headerStyle: {
          backgroundColor: '#FF00CC',
        },
        headerPressColorAndroid: '#FFFFFF',
        headerTintColor: '#FFFFFF',
        headerTitleStyle: {
          fontSize: 18,
          fontWeight: 'bold',
        },
      },
    }
  )
);
