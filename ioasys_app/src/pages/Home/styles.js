import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;
export const ListTile = styled.TouchableOpacity`
  border-bottom-width: 0.5px;
  border-bottom-color: #666;
  width: 100%;
  height: 100px;
  flex-direction: row;
  align-items: center;
  padding: 6px;
`;

export const InfoEnterprise = styled.View`
  justify-content: center;
  padding-left: 18px;
`;

export const EnterpriseName = styled.Text`
  color: #333399;
  font-size: 18px;
  font-weight: bold;
`;

export const City = styled.Text`
  font-size: 14px;
  color: #ff00cc;
`;
export const Country = styled.Text`
  font-size: 14px;
  color: #ff00cc;
`;
