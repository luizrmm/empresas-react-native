import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FlatList, Image, ActivityIndicator } from 'react-native';
import { enterprisesIndex } from '~/store/actions/EnterprisesActions';
import {
  Container,
  ListTile,
  EnterpriseName,
  City,
  Country,
  InfoEnterprise,
} from './styles';
import office from '~/assets/office.png';

export default function SignUp() {
  const dispatch = useDispatch();
  const Enterprises = useSelector(
    state => state.EnterprisesReducer.data.enterprises
  );
  const loading = useSelector(state => state.EnterprisesReducer.loading);
  useEffect(() => {
    dispatch(enterprisesIndex());
  }, []);
  return (
    <Container>
      {loading === true ? (
        <ActivityIndicator size="large" color="#333" />
      ) : (
        <>
          {Enterprises && (
            <FlatList
              keyExtractor={item => String(item.id)}
              data={Enterprises}
              renderItem={({ item }) => (
                <ListTile>
                  <Image
                    defaultSource={office}
                    resizeMode="contain"
                    style={{ height: 80, width: 80 }}
                    source={{
                      uri: `http://empresas.ioasys.com.br${item.photo}`,
                    }}
                  />
                  <InfoEnterprise>
                    <EnterpriseName>{item.enterprise_name}</EnterpriseName>
                    <City>{item.city}</City>
                    <Country>{item.country}</Country>
                  </InfoEnterprise>
                </ListTile>
              )}
            />
          )}
        </>
      )}
    </Container>
  );
}
