import React, { useRef, useState, useEffect } from 'react';
import { Image, Alert } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import Background from '~/components/Background';
import logo from '~/assets/logo.png';
import { SignInAction } from '~/store/actions/SignInActions';

import { Container, Form, FormInput, SubmitButton } from './styles';

export default function SignIn({ navigation }) {
  const dispatch = useDispatch();
  const passwordRef = useRef();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const SignInResponse = useSelector(state => state.SignInReducer.data);
  const ErrorLogin = useSelector(state => state.SignInReducer.error);
  const loading = useSelector(state => state.SignInReducer.loading);

  async function handleSubmit() {
    dispatch(SignInAction(email, password));
  }

  useEffect(() => {
    if (SignInResponse.success === true) {
      navigation.navigate('Home');
    } else if (ErrorLogin === true) {
      Alert.alert('Failed to authenticate', 'incorrect email or password');
    }
  }, [SignInResponse.success, ErrorLogin]);
  return (
    <Background>
      <Container>
        <Image source={logo} resizeMode="contain" style={{ height: 72 }} />
        <Form>
          <FormInput
            icon="mail-outline"
            autoCorrect={false}
            autoCapitalize="none"
            keyboardType="email-address"
            placeholder="Digite seu e-mail"
            returnKeyType="next"
            value={email}
            onChangeText={setEmail}
            onSubmitEditing={() => passwordRef.current.focus()}
          />
          <FormInput
            icon="lock-outline"
            secureTextEntry
            placeholder="Digite sua senha"
            ref={passwordRef}
            returnKeyType="send"
            value={password}
            onChangeText={setPassword}
            onSubmitEditing={handleSubmit}
          />
          <SubmitButton loading={loading} onPress={handleSubmit}>
            ACESSAR
          </SubmitButton>
        </Form>
      </Container>
    </Background>
  );
}
SignIn.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
};
