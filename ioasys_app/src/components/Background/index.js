import LinearGradient from 'react-native-linear-gradient';
import styled from 'styled-components';

export default styled(LinearGradient).attrs({
  colors: ['#FF00CC', '#333399'],
})`
  flex: 1;
`;
