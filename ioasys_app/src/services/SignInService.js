import api from './api';

export function signInService(email, password) {
  return api.post(
    'users/auth/sign_in',
    {
      email,
      password,
    },
    {
      headers: {
        'Content-Type': 'application/json',
      },
    }
  );
}
