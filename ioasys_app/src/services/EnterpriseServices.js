import api from './api';

export function enterprisesIndex() {
  return api.get('enterprises');
}
