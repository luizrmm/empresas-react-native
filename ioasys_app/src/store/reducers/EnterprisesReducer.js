import {
  REQUEST_ENTERPRISES,
  SUCCES_ENTERPRISES,
  FAILURE_ENTERPRISES,
} from '~/store/actions/actionTypes';

const initialState = {
  data: {},
  loading: false,
  error: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_ENTERPRISES:
      return {
        ...state,
        loading: true,
      };
    case SUCCES_ENTERPRISES:
      return {
        data: action.payload.data,
        loading: false,
        error: false,
      };
    case FAILURE_ENTERPRISES:
      return {
        data: {},
        loading: false,
        error: action.payload.data,
      };
    default:
      return state;
  }
};

export default reducer;
