import {
  REQUEST_LOGIN,
  SUCCESS_LOGIN,
  FAILURE_LOGIN,
} from '~/store/actions/actionTypes';

const initialState = {
  data: {},
  loading: false,
  error: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_LOGIN:
      return {
        ...state,
        loading: true,
        error: false,
      };
    case SUCCESS_LOGIN:
      return {
        data: action.payload.data,
        loading: false,
        error: false,
      };
    case FAILURE_LOGIN:
      return {
        data: {},
        loading: false,
        error: action.payload.data,
      };
    default:
      return state;
  }
};

export default reducer;
