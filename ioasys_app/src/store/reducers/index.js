import { combineReducers } from 'redux';
import SignInReducer from './SignInReducer';
import EnterprisesReducer from './EnterprisesReducer';

export default combineReducers({
  SignInReducer,
  EnterprisesReducer,
});
