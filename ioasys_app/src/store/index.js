import { applyMiddleware, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import reducers from './reducers';
import rootSaga from './sagas';

const SagaMiddleware = createSagaMiddleware();

const store = createStore(reducers, applyMiddleware(SagaMiddleware));

SagaMiddleware.run(rootSaga);

export default store;
