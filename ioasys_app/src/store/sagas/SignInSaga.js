import { call, put } from 'redux-saga/effects';
import { signInService } from '~/services/SignInService';
import { SUCCESS_LOGIN, FAILURE_LOGIN } from '~/store/actions/actionTypes';
import api from '~/services/api';

export function* requestSignIn(action) {
  try {
    const Response = yield call(
      signInService,
      action.payload.email,
      action.payload.password
    );
    api.defaults.headers = {
      'access-token': Response.headers['access-token'],
      client: Response.headers.client,
      uid: Response.headers.uid,
    };
    yield put({
      type: SUCCESS_LOGIN,
      payload: {
        data: Response.data,
      },
    });
  } catch (error) {
    yield put({
      type: FAILURE_LOGIN,
      payload: {
        data: true,
      },
    });
  }
}
