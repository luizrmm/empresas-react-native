import { all, takeLatest } from 'redux-saga/effects';
import {
  REQUEST_LOGIN,
  REQUEST_ENTERPRISES,
} from '~/store/actions/actionTypes';
import { requestSignIn } from './SignInSaga';
import { requestEnterprises } from './EnterprisesSaga';

function* root() {
  yield all([
    takeLatest(REQUEST_LOGIN, requestSignIn),
    takeLatest(REQUEST_ENTERPRISES, requestEnterprises),
  ]);
}
export default root;
