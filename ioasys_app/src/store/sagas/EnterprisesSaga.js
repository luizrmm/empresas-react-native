import { call, put } from 'redux-saga/effects';
import { enterprisesIndex } from '~/services/EnterpriseServices';
import {
  SUCCES_ENTERPRISES,
  FAILURE_ENTERPRISES,
} from '~/store/actions/actionTypes';

export function* requestEnterprises() {
  try {
    const Response = yield call(enterprisesIndex);

    yield put({
      type: SUCCES_ENTERPRISES,
      payload: {
        data: Response.data,
      },
    });
  } catch (error) {
    yield put({
      type: FAILURE_ENTERPRISES,
      payload: {
        data: true,
      },
    });
  }
}
