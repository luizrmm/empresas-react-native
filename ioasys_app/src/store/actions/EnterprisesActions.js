import { REQUEST_ENTERPRISES } from './actionTypes';

export function enterprisesIndex() {
  return {
    type: REQUEST_ENTERPRISES,
  };
}
