import { REQUEST_LOGIN } from './actionTypes';

export function SignInAction(email, password) {
  return {
    type: REQUEST_LOGIN,
    payload: {
      email,
      password,
    },
  };
}
